/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/07 19:42:15 by npatton           #+#    #+#             */
/*   Updated: 2018/06/16 23:46:18 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int		readline(char **str, int fd)
{
	int		ret;
	char	*s;
	char	buff[BUFF_SIZE + 1];

	if ((ret = read(fd, buff, BUFF_SIZE)) == -1)
		return (-1);
	buff[ret] = '\0';
	s = *str;
	*str = ft_strjoin(*str, buff);
	if (*s != 0)
		free(s);
	return (ret);
}

static int		mkline(char **str, char **line, char *s)
{
	int		i;
	char	*j;

	i = 0;
	if (*s == '\n')
		i = 1;
	*s = 0;
	*line = ft_strjoin("", *str);
	if (i == 0 && ft_strlen(*str) != 0)
	{
		*str = ft_strnew(1);
		return (1);
	}
	else if (i == 0 && !(ft_strlen(*str)))
		return (0);
	j = *str;
	*str = ft_strjoin(s + 1, "");
	free(j);
	return (i);
}

int				get_next_line(int const fd, char **line)
{
	int			ret;
	char		*s;
	static char	*str;

	if (!line || BUFF_SIZE < 1)
		return (-1);
	ret = BUFF_SIZE;
	if (str == 0)
		str = "";
	while (line)
	{
		s = str;
		while (*s || ret < BUFF_SIZE)
		{
			if (*s == '\n' || *s == 0 || *s == -1)
				return (mkline(&str, line, s));
			s++;
		}
		ret = readline(&str, fd);
		if (ret == -1)
			return (-1);
	}
	return (0);
}
