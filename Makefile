# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: npatton <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/08 11:05:37 by npatton           #+#    #+#              #
#    Updated: 2018/06/17 00:16:18 by npatton          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = get_next_line

CC = clang

FLAGS = -Wall -Werror -Wextra -g

SRC = main.c get_next_line.c

LIB = -L libft/ -lft

$(NAME):
	make -C libft
	$(CC) $(FLAGS) $(SRC) $(LIB) -o $(NAME) 

all: $(NAME)

clean:
	rm -rf *~ \#*\#
	make clean -C libft
	rm -rf *.dSYM

fclean: clean
	rm -rf $(NAME)
	make fclean -C libft

c: clean

f: fclean

re: fclean all

r: re

