/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/07 19:48:42 by npatton           #+#    #+#             */
/*   Updated: 2018/06/17 00:24:29 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		main(int ac, char **av)
{
	int		fd;
	char	*line;
	int		i;

	i = 1;
	if (ac < 2)
	{
		fd = 0;
		return -1;
	}
	else
	{
		while (i < ac)
		{
			fd = open(av[i], O_RDONLY);
			while(get_next_line(fd, &line) == 1)
			{
				ft_putendl(line);
				free(line);
			}
			i++;
		}
	}
	close(fd);
}
